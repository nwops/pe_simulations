# PE Simulation for a YAML environment setup
Example setup for use with r10k yaml environments.  This enforces a [trunk based development workflow](https://trunkbaseddevelopment.com/) instead of a promotion based workflow which has been the default since r10k's initial release.  This simulation will setup an exact replica of that environment and will give you the oppourntity to try it out before implementing trunk based development yourself.

Uses [as the r10k remote source](https://github.com/nwops/kontrol-repo-yaml)

Uses [as the code manager sources](https://github.com/nwops/kontrol-repo-yaml/blob/main/data/common.yaml) for dynamic and statically defined environments.

[Script](https://github.com/nwops/puppet-environments/blob/main/r10k-environments-git.sh) for keeping the environments yaml file in VCS.

All of these things should be auto setup with this simulation.  You can run the `puppet code deploy --all -w` to ensure everything works.

## Requirements for you environment
1. Must have a production branch on the control repo (for initial setup)
2. Must have the execution script for producing the yaml content on the server under /etc/puppetlabs/r10k/r10k-environments.sh
3. Script must execute permissions for pe-puppet
4. If url is git based, pe-puppet will need permission to use ssh keys
   
