#!/usr/bine/env bash
docker exec -ti pe-std-yaml.puppet.vm yum install git -y
docker exec -ti pe-std-yaml.puppet.vm mkdir -p /etc/puppetlabs/r10k/
docker exec -ti pe-std-yaml.puppet.vm curl https://raw.githubusercontent.com/nwops/puppet-environments/main/r10k-environments-git.sh -o /etc/puppetlabs/r10k/r10k-environments-git.sh
docker exec -ti pe-std-yaml.puppet.vm chmod +x /etc/puppetlabs/r10k/r10k-environments-git.sh

