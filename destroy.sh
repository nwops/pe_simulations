#!/usr/bin/env bash
# must have bolt 3.18+ installed
name=$(basename $PWD)
inventory_dir=$(realpath ./)
inventory_path=${inventory_dir}/inventory.yaml
echo 'Please choose a PE architecture to destroy: '
select opt in */
do
  dir=$(realpath ${opt})
  name=$(basename $opt)
  docker-compose --project-directory=$dir down
  break;
done
