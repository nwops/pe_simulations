#!/usr/bin/env bash
# must have bolt 3.18+ installed
name=$(basename $PWD)
inventory_dir=$(realpath ./)
inventory_path=${inventory_dir}/inventory.yaml
echo 'Please choose a PE architecture to provision: '
select opt in */
do
  dir=$(realpath ${opt})
  name=$(basename $opt)
  break;
done
echo "Select method of installation"
select method in install upgrade; do
  case $method in
     install)
       echo "Installing PE"
       params_file=params.json
       plan="peadm::install"
       pre_plan_file=pre-install.sh
       post_plan_file=post-install.sh
       break
       ;;
     upgrade)
       echo "Upgrading PE"
       params_file=upgrade_params.json
       plan="peadm::upgrade"
       pre_plan_file=pre-upgrade.sh
       post_plan_file=post-upgrade.sh
       break
       ;;
     *)
      echo "invalid choice"
      exit 1
      ;;
  esac
done


bolt module install
cd $dir
# The concurrency is set to 2 to keep CPU usage from skyrocketing during Large and XL deployments
docker-compose up -d --build 
if [[ -f $pre_plan_file ]]; then 
  bash $pre_plan_file
fi
bolt plan run ${plan} --concurrency 2 --params @${params_file} --targets=$name
if [[ -f $post_plan_file ]]; then 
  bash $post_plan_file
fi
cd -
